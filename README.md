This is the server for our nat traversal and TCP congestion control testing excersice.
Users connect to this server on port 1400 and send their requests containing thier personal tokens
as identifier. There are this requests available: (in order to make tests feasible we provided each student with two token)

* `REGISTER:[token]:UserName` --> Creates a user named UserName associateted with token and records its ip and port

* `HEARTBEAT:[token]` --> Updates user availability timestamp to 1min later than the time request is recieved

* `INVITE:[token]:PeerName` --> If peer is available it send a message determining chat start to both of them containing their respective ips and ports

* `MAKE_RELAY:[token]:PeerName` -->  If peer is available it send a message to him asking if he is interested in opening a relay and if he answers with an `ACK_REL:[token]:PeerName` it will creates a new relay server process with a random session key and sends this session key to both users. they can communicate with messages like `RELAY:[session_key]:body` with each other since then!

* `TERMINATE:[token]:session_key` --> Closes relay session between the two peers.