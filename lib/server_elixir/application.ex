defmodule ServerElixir.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Starts a worker by calling: ServerElixir.Worker.start_link(arg)
      # {ServerElixir.Worker, arg},
      {DynamicSupervisor, name: ServerElixir.DataSupervisor, strategy: :one_for_one},
      {Task.Supervisor, name: ServerElixir.TaskSupervisor},
      Supervisor.child_spec({Task, fn -> UdpServer.launch_server(13570) end}, restart: :permanent)
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_all, name: ServerElixir.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
