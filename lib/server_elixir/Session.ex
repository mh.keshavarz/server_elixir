defmodule ServerElixir.Session do
  use Agent, restart: :temporary

  def start_link(_opts) do
    Agent.start_link(fn -> %{} end)
  end

  def send(session, session_key, message, sender_ip, server) do
    if ServerElixir.Session.is_alive?(session) do
      to_ip = ServerElixir.Session.get_peer_ip(session, sender_ip)
      if to_ip != :NoMatch do
        message_compiled = "RELAYED:"<>session_key<>message
        Socket.Datagram.send!(server, message_compiled, to_ip)
      else
        :IpIsNotValid
      end
    end
  end

  def set(session, session_key, ip1, port1, ip2, port2) do
    Agent.update(session, fn _state -> %{
      'session_key' => session_key,
      'user1'=> [ip1, port1],
      'user2' => [ip2, port2],
      'Alive'=> Time.utc_now(),
      'Active' => false
    } end)
  end

  def get_peer_ip(session, ip) do
    [ip1, port1] = Agent.get(session, &Map.get(&1, 'user1'))
    [ip2, port2] = Agent.get(session, &Map.get(&1, 'user2'))
    IO.puts(inspect(ip1))
    case ip do
      ^ip1 -> {ip1, port1}
      ^ip2 -> {ip2, port2}
      _ -> :NoMatch
    end
  end

  def heartbeat(session) do
    Agent.update(session, &Map.put(&1, 'Alive', Time.utc_now()))
  end

  def is_alive?(session) do
    case Agent.get(session, &Map.get(&1, 'Alive')) |> Time.compare(Time.add(Time.utc_now(), -60*10)) do
      :gt -> :true
      _ -> :false
    end
  end

  def activate(session) do
    Agent.update(session, &Map.put(&1, 'Active', true))
  end

  def is_active(session) do
    Agent.get(session, &Map.get(&1, 'Active'))
  end
end
