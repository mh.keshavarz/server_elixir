defmodule ServerElixir.User do
  use Agent, restart: :temporary

  def start_link(_opts) do
    Agent.start_link(fn -> %{} end)
  end

  def get_username(user) do
    Agent.get(user, &Map.get(&1, 'name'))
  end

  def get_ip(user) do
    {ip, _} = Agent.get(user, &Map.get(&1, 'client'))
    ip
  end

  def get_client(user) do
    Agent.get(user, &Map.get(&1, 'client'))
  end

  def get_port(user) do
    {_, port} = Agent.get(user, &Map.get(&1, 'client'))
    port
  end

  def get_token(user) do
    Agent.get(user, &Map.get(&1, 'token'))
  end

  def set(user, token, name, ip, port) do
    Agent.update(user, fn _state -> %{
      'token' => token,
      'name'=> name,
      'client' => {ip, port},
      'Alive'=> Time.utc_now()
    } end)
  end

  def heartbeat(user) do
    Agent.update(user, &Map.put(&1, 'Alive', Time.utc_now()))
  end

  def is_alive?(user) do
    case Agent.get(user, &Map.get(&1, 'Alive')) |> Time.compare(Time.add(Time.utc_now(), -60)) do
      :gt -> :true
      _ -> :false
    end
  end
end
