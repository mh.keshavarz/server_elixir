defmodule ServerElixir do
  use GenServer

  @tokens ["123" , "456"]
  ## server starter
  def start_link(opts) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  ## client interface
  def register(server, token, username, ip, port) do
    GenServer.call(server, {:register, token, username, ip, port})
  end

  def heartbeat(server, token) do
    GenServer.cast(server, {:heartbeat, token})
  end

  def invite(server, token, peername, sock_ser) do
    GenServer.call(server, {:invite, token, peername, sock_ser})
  end

  def make_relay(server, token, peername, sock_ser) do
    GenServer.call(server, {:make_relay, token, peername, sock_ser})
  end

  def ack_relay(server, token, peername, sock_ser) do
    GenServer.call(server, {:ack_rel, token, peername, sock_ser})
  end

  def relay(server, session, ip, message, sock_ser) do
    GenServer.call(server, {:relay, session, ip, message, sock_ser})
  end

  def teminate(server, token, session) do
    GenServer.call(server, {:terminate, token, session})
  end

  ## Server callbacks
  def init(:ok) do
    users_b_t = %{}
    users_b_n = %{}
    sessions = %{}
    refs = %{}
    {:ok, [users_b_t, users_b_n, sessions, refs]}
  end

  def handle_call({:register, token, username, ip, port}, _from, [users_b_t, users_b_n , sessions, refs]) do
    state = [users_b_t, users_b_n, sessions, refs]
    if Map.has_key?(users_b_t, token) do
      if Map.has_key?(users_b_n, username) do
        {:reply, :DuplicateUserName, state}
      else
        users_b_n = Map.delete(users_b_n,
        Map.get(users_b_t, token)
        |> ServerElixir.User.get_username())

        users_b_n = Map.put(users_b_n, username, token)
        Map.get(users_b_t, token)
        |> ServerElixir.User.set(token, username, ip, port)
        {:reply, :Registered, [users_b_t, users_b_n, sessions, refs]}
      end
    else
      if Enum.member?(@tokens, token) do
        users_b_n = Map.put(users_b_n, username, token)
        {:ok, user} = DynamicSupervisor.start_child(ServerElixir.DataSupervisor, ServerElixir.User)
        # {:ok, user} = ServerElixir.User.start_link([])
        ServerElixir.User.set(user, token, username, ip, port)
        users_b_t = Map.put(users_b_t, token, user)
        {:reply, :Registered, [users_b_t, users_b_n, sessions, refs]}
      else
        {:reply, :UnAuthorized, state}
      end
    end
  end

  def handle_call({:invite, token, peername, server}, _from, [users_b_t, users_b_n , sessions, refs]) do
    if Map.has_key?(users_b_t, token) do
      if Map.has_key?(users_b_n, peername) do
        user1 = Map.get(users_b_t, token)
        ServerElixir.User.heartbeat(user1)
        user2 = Map.get(users_b_t, Map.get(users_b_n, peername))
        if ServerElixir.User.is_alive?(user1) and ServerElixir.User.is_alive?(user2) do
          Socket.Datagram.send(server,
           "START:"<>inspect(ServerElixir.User.get_client(user2)),
            ServerElixir.User.get_client(user1))
          Socket.Datagram.send(server,
            "START:"<>inspect(ServerElixir.User.get_client(user1)),
             ServerElixir.User.get_client(user2))
          {:reply, :INVITED, [users_b_t, users_b_n , sessions, refs]}
        else
          {:reply, :PeerNotFound, [users_b_t, users_b_n , sessions, refs]}
        end
      else
        {:reply, :PeerNotFound, [users_b_t, users_b_n , sessions, refs]}
      end
    else
      {:reply, :UnAuthorized, [users_b_t, users_b_n , sessions, refs]}
    end
  end

  def handle_call({:make_relay, token, peername, server}, _from, [users_b_t, users_b_n , sessions, refs]) do
    if Map.has_key?(users_b_t, token) do
      if Map.has_key?(users_b_n, peername) do
        user1 = Map.get(users_b_t, token)
        username_1 = ServerElixir.User.get_username(user1)
        user2_token = Map.get(users_b_n, peername)
        user2 = Map.get(users_b_t, user2_token)
        sess_string = token <> user2_token
        session_key = :crypto.hash(:md5, sess_string) |> Base.encode16()
        ## send rel_req to this
        Socket.Datagram.send(server,
        "RELREQ_FROM:#{username_1}",
         ServerElixir.User.get_client(user2))
        sessions = Map.delete(sessions, session_key)
        {:ok, session} = DynamicSupervisor.start_child(ServerElixir.DataSupervisor, ServerElixir.Session)
        # {:ok, session} = ServerElixir.Session.start_link([])
        ServerElixir.Session.set(session, session_key,
         ServerElixir.User.get_ip(user1),
         ServerElixir.User.get_port(user1),
         ServerElixir.User.get_ip(user2),
         ServerElixir.User.get_port(user2))
        sessions = Map.put(sessions, session_key, session)
        {:reply, :Success, [users_b_t, users_b_n , sessions, refs]}
      else
        {:reply, :PeerNotFound, [users_b_t, users_b_n , sessions, refs]}
      end
    else
      {:reply, :UnAuthorized, [users_b_t, users_b_n , sessions, refs]}
    end
  end

  def handle_call({:ack_rel, token, peername, server}, _from, [users_b_t, users_b_n , sessions, refs]) do
    if Map.has_key?(users_b_t, token) do
       if Map.has_key?(users_b_n, peername) do
        session_key = :crypto.hash(:md5, Map.get(users_b_n, peername) <> token) |> Base.encode16()
        if Map.has_key?(sessions, session_key) do
          user1 = Map.get(users_b_t, token)
          username_1 = ServerElixir.User.get_username(user1)
          user2_token = Map.get(users_b_n, peername)
          user2 = Map.get(users_b_t, user2_token)
          Socket.Datagram.send(server,
          "RELTO:#{peername}:#{session_key}",
           ServerElixir.User.get_client(user2))
          Socket.Datagram.send(server,
          "RELTO:#{username_1}:#{session_key}",
            ServerElixir.User.get_client(user2))
          Map.get(sessions, session_key)
          |> ServerElixir.Session.activate()
          {:reply, :Success, [users_b_t, users_b_n , sessions, refs]}
        else
          {:reply, :InvalidSession, [users_b_t, users_b_n , sessions, refs]}
        end
       else
        {:reply, :PeerNotFound, [users_b_t, users_b_n , sessions, refs]}
      end
    else
      {:reply, :UnAuthorized, [users_b_t, users_b_n , sessions, refs]}
    end
  end

  def handle_call({:relay, session, ip, message, server}, _from, [users_b_t, users_b_n , sessions, refs]) do
    if Map.has_key?(sessions, session) do
      Map.get(sessions, session)
      |> ServerElixir.Session.send(session, message, ip, server)
      {:reply, :Relayed, [users_b_t, users_b_n , sessions, refs]}
    else
      {:reply, :UnAuthorized, [users_b_t, users_b_n , sessions, refs]}
    end
  end

  def handle_call({:terminate, token, session}, _from, [users_b_t, users_b_n , sessions, refs]) do
    if Map.has_key?(sessions, session)  and Map.has_key?(users_b_t, token) do
      Map.get(sessions, session)
      |> Process.exit(:terminate)
      sessions = Map.delete(sessions, session)
      IO.puts(inspect(users_b_n))
      IO.puts(inspect(users_b_t))
      {:reply, :Terminated, [users_b_t, users_b_n , sessions, refs]}
    else
      {:reply, :SessionNotFound, [users_b_t, users_b_n , sessions, refs]}
    end
  end

  def handle_cast({:heartbeat, token}, [users_b_t, users_b_n , sessions, refs]) do
    if Map.has_key?(users_b_t, token) do
      Map.get(users_b_t, token)
      |> ServerElixir.User.heartbeat()
    end
      {:noreply,[users_b_t, users_b_n , sessions, refs]}
  end
end

