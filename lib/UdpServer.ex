defmodule UdpServer do
  @moduledoc """
  Documentation for UdpClientServer.
  """

  @default_server_port 13570

  def launch_server do
    launch_server(@default_server_port)
  end

  def launch_server(port) do
    IO.puts "Launching server on localhost on port #{port}"
    server = Socket.UDP.open!(port)
    # {:ok, command_server} = ServerElixir.start_link([])
    {:ok, command_server} = Supervisor.start_child(ServerElixir.Supervisor,   %{
      id: ServerElixirRegistry,
      start: {ServerElixir, :start_link, [[]]}
    })
    # {:ok, command_server} = DynamicSupervisor.start_child(ServerElixir.RegSupervisor, ServerElixir)
    accept(server, command_server)
  end

  def accept(server, command_server) do
    {command, client} = server |> Socket.Datagram.recv!
    IO.puts "Received: #{command}, from #{inspect(client)}"
    Task.Supervisor.start_child(ServerElixir.TaskSupervisor,
                                fn -> serve(server, command_server, command, client) end)
    accept(server, command_server)
  end

  def serve(server, command_server, command, client) do

    case UdpServer.run(server, command_server, command, client) do
      :DuplicateUserName -> send_response(server, client, "DuplicateUserName")
      :Registered -> send_response(server, client, "Registered")
      :UnAuthorized -> send_unauth(server, client)
      :PeerNotFound -> send_response(server, client, "PeerNotFound")
      :INVITED -> :INVITED
      :UnknownCommand -> :UnknownCommand
      :InvalidSession -> send_response(server, client, "InvalidSession")
      :Terminated -> send_response(server, client, "Terminated")
      :SessionNotFound -> send_response(server, client, "SessionNotFound")
      :Relayed -> send_response(server, client, "RELAYED")
      :Success -> send_response(server, client, "SUCCESS")
      _ -> send_parse_error(server, client)
    end
  end

  def run(serv, command_server, command, client) do
    {ip, port} = client
    case String.split(command, ":") do
      ["REGISTER", token, name] -> ServerElixir.register(command_server, token, name, ip, port)
      ["HEARTBEAT", token] -> ServerElixir.heartbeat(command_server, token)
      ["INVITE", token, peername] -> ServerElixir.invite(command_server, token, peername, serv)
      ["MAKE_RELAY", token, peername] -> ServerElixir.make_relay(command_server, token, peername, serv)
      ["ACK_RELAY", token, peername] -> ServerElixir.ack_relay(command_server, token, peername, serv)
      ["RELAY", session_key|body] -> ServerElixir.relay(command_server, session_key, ip, List.to_string(body), serv)
      ["TERMINATE", token, session_key] -> ServerElixir.teminate(command_server, token, session_key)
      _ -> UdpServer.send_unknown_error(serv, client)
    end
  end

  def send_unknown_error(serv, client) do
    Socket.Datagram.send(serv, "UnknownCommand", client)
    :UnknownCommand
  end

  def send_parse_error(serv, client) do
    Socket.Datagram.send(serv, "Unable to parse", client)
  end

  def send_response(serv, client, message) do
    Socket.Datagram.send(serv, message, client)
  end

  def send_unauth(serv, client) do
    Socket.Datagram.send(serv, "UnAuthorized", client)
  end
end
